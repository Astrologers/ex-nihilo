package exnihilo.registries;

import java.util.HashMap;
import java.util.HashSet;

import exnihilo.ENBlocks;
import exnihilo.ENItems;
import exnihilo.Fluids;
import exnihilo.data.ModData;
import exnihilo.registries.helpers.EntityWithItem;
import exnihilo.registries.helpers.FluidItemCombo;
import exnihilo.utils.ItemInfo;

import net.minecraft.entity.monster.EntityBlaze;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;

public class BarrelRecipeRegistry {
	
	private static HashMap<FluidItemCombo, ItemInfo> recipes = new HashMap<FluidItemCombo, ItemInfo>();
	private static HashSet<ItemInfo> renderOverride = new HashSet<ItemInfo>();
	private static HashMap<FluidItemCombo, EntityWithItem> mobRecipes = new HashMap<FluidItemCombo, EntityWithItem>();
	
	
	/**
	 * Adds a new "basic" barrel recipe, using a fluid and an input item
	 * @param fluid Input Fluid
	 * @param inputStack Input ItemStack
	 * @param outputStack Output ItemStack
	 */
	public static void addFluidItemRecipe(Fluid fluid, ItemStack inputStack, ItemStack outputStack)
	{
		addFluidItemRecipe(fluid, inputStack, outputStack, false);
	}
	
	/**
	 * Adds a new "basic" barrel recipe, using a fluid and an input item
	 * @param fluid Input Fluid
	 * @param inputStack Input ItemStack
	 * @param outputStack Output ItemStack
	 * @param override Rendering override. If true, the output block won't be rendered. Useful for items that are not full blocks
	 */
	public static void addFluidItemRecipe(Fluid fluid, ItemStack inputStack, ItemStack outputStack, boolean override)
	{
		recipes.put(new FluidItemCombo(fluid, inputStack), new ItemInfo(outputStack));
		if (override)
			renderOverride.add(new ItemInfo(outputStack));
	}
	
	/**
	 * Removes a "basic" barrel recipe
	 * @param fluid Input Fluid
	 * @param inputStack Input ItemStack
	 * @param outputStack Output ItemStack
	 */
	public static void removeFluidItemRecipe(Fluid fluid, ItemStack inputStack, ItemStack outputStack)
	{
		recipes.remove(new ItemInfo(outputStack));
		renderOverride.remove(new ItemInfo(outputStack));
	}
	
	public static ItemInfo getOutput(FluidStack fluid_, ItemStack inputStack)
	{
		Fluid fluid;
		if (fluid_ == null)
			fluid = null;
		else
			fluid = fluid_.getFluid();
		
		return recipes.get(new FluidItemCombo(fluid, inputStack));
	}
	
	public static boolean getShouldRenderOverride(ItemStack stack)
	{
		return renderOverride.contains(new ItemInfo(stack));
	}
	
	/**
	 * Adds a new recipe to spawn mobs using a barrel
	 * @param fluid Input fluid
	 * @param inputStack Input ItemStack
	 * @param entity Class of the entity to be spawned. NOTE: Must have a constructor that takes {@link net.minecraft.World} only.
	 * @param particleName Particles to spawn while the mob is "summoning"
	 * @param peacefulDrop ItemStack to give in the event that the world is peaceful
	 */
	@SuppressWarnings("rawtypes")
	public static void addMobRecipe(Fluid fluid, ItemStack inputStack, Class entity, String particleName, ItemStack peacefulDrop)
	{
		mobRecipes.put(new FluidItemCombo(fluid, inputStack), new EntityWithItem(entity, peacefulDrop, particleName));
	}
	
	/**
	 * Removes a given recipe for a mob
	 * @param fluid Input fluid
	 * @param inputStack Input ItemStack
	 */
	public static void removeMobRecipe(Fluid fluid, ItemStack inputStack)
	{
		mobRecipes.remove(new FluidItemCombo(fluid, inputStack));
	}
	
	public static EntityWithItem getMobOutput(FluidStack fluid_, ItemStack inputStack)
	{
		Fluid fluid;
		if (fluid_ == null)
			fluid = null;
		else
			fluid = fluid_.getFluid();
		
		return mobRecipes.get(new FluidItemCombo(fluid, inputStack));
	}
	
	public static void registerBaseRecipes()
	{
		if (ModData.ALLOW_BARREL_RECIPE_CLAY)
			addFluidItemRecipe(FluidRegistry.WATER, new ItemStack(ENBlocks.Dust), new ItemStack(Blocks.clay));
		if (ModData.ALLOW_BARREL_RECIPE_NETHERRACK)
			addFluidItemRecipe(FluidRegistry.LAVA, new ItemStack(Items.redstone), new ItemStack(Blocks.netherrack));
		if (ModData.ALLOW_BARREL_RECIPE_ENDSTONE)
			addFluidItemRecipe(FluidRegistry.LAVA, new ItemStack(Items.glowstone_dust), new ItemStack(Blocks.end_stone));
		if (ModData.ALLOW_BARREL_RECIPE_SOULSAND)
			addFluidItemRecipe(Fluids.fluidWitchWater, new ItemStack(Blocks.sand), new ItemStack(Blocks.soul_sand));
		if (ModData.ALLOW_BARREL_RECIPE_DARK_OAK)
			addFluidItemRecipe(Fluids.fluidWitchWater, new ItemStack(Blocks.sapling), new ItemStack(Blocks.sapling, 1, 5), true);
		if (ModData.ALLOW_BARREL_RECIPE_DOUBLE_FLOWERS)
		{
			addFluidItemRecipe(Fluids.fluidWitchWater, new ItemStack(Blocks.yellow_flower), new ItemStack(Blocks.double_plant), true);
			addFluidItemRecipe(Fluids.fluidWitchWater, new ItemStack(Blocks.red_flower, 1, 2), new ItemStack(Blocks.double_plant, 1, 1), true);
			addFluidItemRecipe(Fluids.fluidWitchWater, new ItemStack(Blocks.tallgrass, 1, 1), new ItemStack(Blocks.double_plant, 1, 2), true);
			addFluidItemRecipe(Fluids.fluidWitchWater, new ItemStack(Blocks.tallgrass, 1, 2), new ItemStack(Blocks.double_plant, 1, 3), true);
			addFluidItemRecipe(Fluids.fluidWitchWater, new ItemStack(Blocks.red_flower), new ItemStack(Blocks.double_plant, 1, 4), true);
			addFluidItemRecipe(Fluids.fluidWitchWater, new ItemStack(Blocks.red_flower, 1, 3), new ItemStack(Blocks.double_plant, 1, 5), true);
		}
		
		Fluid seedOil = FluidRegistry.getFluid("seedoil");
		if (seedOil != null)
			addFluidItemRecipe(seedOil, new ItemStack(ENBlocks.BeeTrap), new ItemStack(ENBlocks.BeeTrapTreated));
		
		//Mobs
		if (ModData.ALLOW_BARREL_RECIPE_BLAZE_RODS)
			addMobRecipe(FluidRegistry.LAVA, new ItemStack(ENItems.DollAngry), EntityBlaze.class, "lava", new ItemStack(Items.blaze_rod));
		if (ModData.ALLOW_BARREL_RECIPE_ENDER_PEARLS)
			addMobRecipe(Fluids.fluidWitchWater, new ItemStack(ENItems.DollCreepy), EntityEnderman.class, "portal", new ItemStack(Items.ender_pearl));
	}

}
