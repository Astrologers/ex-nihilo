package exnihilo.registries.helpers;

import lombok.EqualsAndHashCode;
import net.minecraft.block.Block;

@EqualsAndHashCode
public class HeatSource {
	public Block block;
	public int meta;
	public float value;
	
	public HeatSource(Block block, int meta, float value)
	{
		this.block = block;
		this.meta = meta;
		this.value = value;
	}
}
