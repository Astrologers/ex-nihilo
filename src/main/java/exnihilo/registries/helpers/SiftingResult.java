package exnihilo.registries.helpers;

import net.minecraft.item.Item;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SiftingResult {
	
	public Item item;
	public int meta;
	public int rarity;

}
